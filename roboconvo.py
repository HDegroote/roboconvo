import argparse
from pathlib import Path

from roboconvo_helpers import create_robot_speech


def main():
    parser = argparse.ArgumentParser(description='Turn written text into conversing robots')
    parser.add_argument('--conversation',
                        type=str,
                        help='The .txt file with the conversation to be acted out ('
                             'each line of conversation should start with a character identifying the speaker)')
    parser.add_argument('--refspeech',
                        type=str,
                        help='The .wav file with the reference speech',
                        default=None)
    parser.add_argument('--resdir',
                        type=str,
                        help='The directory where the resulting audio files should be created. '
                             'The directory will be created. See also -o overwrite option')
    parser.add_argument('-o', '--overwrite',
                        help='Proceed even if the result directory already exists. '
                             'Old files will be overwritten by new files with the same name.',
                        action="store_true")

    args = parser.parse_args()
    file_with_conversation = Path(args.conversation)
    file_with_reference_speech = args.refspeech
    res_dir = Path(args.resdir)
    overwrite_resdir = args.overwrite

    create_robot_speech(file_with_reference_speech=file_with_reference_speech,
                        file_with_source=file_with_conversation,
                        res_dir=res_dir,
                        overwrite_resdir=overwrite_resdir)


main()