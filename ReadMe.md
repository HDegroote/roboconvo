# RoboConvo

Make robot voices act out dialogues

### Functionality
The intended functionality is to have speech synthesisers
read out text, thus simulating conversation.

The program takes a .txt file with a conversation as input (each line starts
with a character indicating the voice to be used, see the [example](example/conversation.txt)) and
an optional .wav file with reference speech.

The program outputs a series of .wav (sound) files. Each .wav file contains the pronunciation of
one line of the input conversation.

### Setup

    pip install -r requirements.txt

Note: tested on Python 3.8

### Usage

    python roboconvo.py --conversation="example/conversation.txt" --refspeech="example/reference.wav" --resdir="example/result"

or without the reference speech:

    python roboconvo.py --conversation="example/conversation.txt" --resdir="example/result"


Add `--overwrite` if you don't mind if the result directory already exists

Note: downloading the model and generating the audio might take a few minutes

### Limitations/Extensions
Some easy extensions to make are:
- Support for more voices (currently max 2: with 1 female (F) and 1 male (M) voice)
- Conversations between more than two participants 
- Use a distinct reference speech for each voice (currently same is used for all voices, which makes little sense)
- Put all conversation snippets in the same .wav file

Another extension is to offer support for arbitrary voice selection amongst the >1000 available voices.

Feel free to open an issue if you are interested in either of these extensions.


### Disclaimer
The code is built around this [demo](https://colab.research.google.com/github/espnet/notebook/blob/master/espnet2_tts_realtime_demo.ipynb)
by Tomoki Hayashi. 
