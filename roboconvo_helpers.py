# Based on the following demo by Tomoki Hayashi:
#   https://colab.research.google.com/github/espnet/notebook/blob/master/espnet2_tts_realtime_demo.ipynb)
from collections import defaultdict
from pathlib import Path
import os
import kaldiio
from scipy.io.wavfile import write as write_wav
import time
import torch
from espnet_model_zoo.downloader import ModelDownloader
from espnet2.bin.tts_inference import Text2Speech
from parallel_wavegan.utils import download_pretrained_model
from parallel_wavegan.utils import load_model
import soundfile as sf
import logging

logger = logging.getLogger(__name__)

# Parameters you probably need to change
# RES_DIR = "result"
# OVERWRITE_EXISTING_RES_DIR = True
# FILE_WITH_CONVERSATION = "Genders identified.txt"
# REF_SPEECH_LOCATION = "Jerzy Kosinski reference3.wav"  # set None for no reference speech

# Note: currently the same reference speech is used for all voices.
# This makes no sense: the code should be adapted to have one reference speech per voice used

SPEAKER_TO_VOICE_ID_MAP = {
    "M": '5189_56574',
    "F": '2817_142380'
}

LANGUAGE = 'English'
SAMPLE_RATE = 24000  # also known as Fs
# NOTE: the sample rate of the reference audio should match this sample rate


DEVICE = "cpu"  # or gpu, but that option is untested

#  NOTE: the model works with a mono track.
#  If the reference is stereo, ideally just normalise the audio to mono
#  If you don't know how to do that (it's easy with a free tool like audacity)
#   or want a quick fix, set this variable to True and an ad-hoc fix will be applied
#   by only considering the audio in the first track
IS_REF_SPEECH_STEREO = False

# See reference demo for a  list of other options for the model
MODEL_TAG = 'kan-bayashi/libritts_gst+xvector_conformer_fastspeech2'

# See reference demo for a  list of other options for the vocoder
VOCODER_TAG = "libritts_parallel_wavegan.v1.long"  # @param [


def create_robot_speech(
        file_with_source,
        file_with_reference_speech,
        res_dir,
        overwrite_resdir=False
    ):
    res_dir = Path(res_dir)
    file_with_source = Path(file_with_source)
    if file_with_reference_speech is not None:
        file_with_reference_speech = Path(file_with_reference_speech)
    res_dir.mkdir(exist_ok=overwrite_resdir)

    text2speech = load_text2speech_model(model_tag=MODEL_TAG,
                                         device=DEVICE)
    vocoder = load_vocoder(vocoder_tag=VOCODER_TAG,
                           device=DEVICE)
    speaker_id_to_voice_map = load_speaker_id_to_voice_map(model_tag=MODEL_TAG)
    ref_speech = get_ref_speech(text2speech_model=text2speech,
                                ref_speech_location=file_with_reference_speech,
                                reference_sample_rate=SAMPLE_RATE,
                                is_stereo=IS_REF_SPEECH_STEREO)
    phrases_per_speaker = read_file_with_conversation(file_with_source)

    for speaker, phrases in phrases_per_speaker.items():
        voice_id = SPEAKER_TO_VOICE_ID_MAP.get(speaker, None)
        if voice_id is None:
            logger.warning(f"Cannot generate voices for speaker '{speaker}' because no entry"
                           f"was found in the speaker_to_voice_id map. Please add such an entry."
                           f"\n\t---Skipping audio generation for speaker '{speaker}'")
        else:
            voice_vector = speaker_id_to_voice_map[voice_id]
            for i, phrase in enumerate(phrases, 1):
                result = transform_text_to_speech(text=phrase,
                                                  voice_vector=voice_vector,
                                                  reference_speech=ref_speech,
                                                  text2speech_model=text2speech,
                                                  vocoder=vocoder)
                filename = res_dir / (speaker + "-" + str(i) + ".wav")
                write_wav(filename, SAMPLE_RATE, result)


def load_text2speech_model(model_tag,
                           device,
                           speed_control_alpha=1):
    d = ModelDownloader()
    text2speech = Text2Speech(
        **d.download_and_unpack(model_tag),
        device=device,
        # Only for Tacotron 2
        threshold=0.5,
        minlenratio=0.0,
        maxlenratio=10.0,
        use_att_constraint=False,
        backward_window=1,
        forward_window=3,
        # Only for FastSpeech & FastSpeech2
        speed_control_alpha=speed_control_alpha,
    )
    text2speech.spc2wav = None  # Disable griffin-lim
    return text2speech


def load_vocoder(vocoder_tag, device):
    # NOTE [by original author of the demo]:
    #   Sometimes download is failed due to "Permission denied". That is
    #   the limitation of google drive. Please retry after serveral hours.
    vocoder = load_model(download_pretrained_model(vocoder_tag)).to(device).eval()
    vocoder.remove_weight_norm()
    return vocoder


def load_speaker_id_to_voice_map(model_tag):
    d = ModelDownloader()
    model_dir = os.path.dirname(d.download_and_unpack(model_tag)["train_config"])
    xvector_ark = f"{model_dir}/../../dump/xvector/tr_no_dev/spk_xvector.ark"  # training speakers

    # NOTE: if you want more speakers, feel free to also add the dev and eval speakers
    # xvector_ark = f"{model_dir}/../../dump/xvector/dev/spk_xvector.ark"  # development speakers
    # xvector_ark = f"{model_dir}/../../dump/xvector/eval1/spk_xvector.ark"  # eval speakers
    xvectors = {k: v for k, v in kaldiio.load_ark(xvector_ark)}
    return xvectors


def get_ref_speech(text2speech_model,
                   ref_speech_location,
                   reference_sample_rate,
                   is_stereo=False,
                   ):
    if text2speech_model.use_speech:
        if ref_speech_location is None:
            logger.warning("No reference speech was defined--will use a random reference (note:"
                           "the randomness is deterministic, so each execution of this program uses the "
                           "same random reference)")
            samples_to_generate = 50000
            fixed_seed = 123456789
            generator = torch.Generator()
            generator.manual_seed(fixed_seed)  # deterministic random
            speech = torch.randn(samples_to_generate, generator=generator)
        else:
            speech, sample_rate = sf.read(ref_speech_location)
            if not sample_rate == reference_sample_rate:
                raise ValueError("The reference audio has a different sample rate than that selected "
                                 f"for the model ({sample_rate} in reference, "
                                 f"{reference_sample_rate} for the model)."
                                 f" Please ensure that the sample rates are equal (or set the reference"
                                 f" to null for a quick fix by not using any).")

            speech = torch.from_numpy(speech).float()

            if is_stereo:
                speech = speech[:, 0]  # make mono if not (this is an ad hoc fix, better to make the audio mono)
        return speech
    return None


def standard_cleaner_func(line):
    line = line.replace("microphones", "micro phones")  # strange bug: microphones unpronouncable
    line = line.replace("—", ", ")  # Em dash not read as pause
    return line


def read_file_with_conversation(file_location, cleaner_func=None):
    with open(file_location) as file:
        lines = file.readlines()

    lines_per_speaker = defaultdict(list)

    for line in lines:
        if len(line.strip()) > 0:
            speaker = line[0]
            line_content = line[2:].strip()
            if cleaner_func is not None:
                line_content = cleaner_func(line_content)
            lines_per_speaker[speaker].append(line_content)
    return lines_per_speaker


def transform_text_to_speech(
        text,
        voice_vector,
        text2speech_model,
        reference_speech,
        vocoder
):
    # synthesis
    with torch.no_grad():
        start = time.time()
        wav, c, *_ = text2speech_model(text,
                                       speech=reference_speech,
                                       spembs=voice_vector)
        wav = vocoder.inference(c)
    rtf = (time.time() - start) / (len(wav) / SAMPLE_RATE)
    logger.info(f"RTF = {rtf:5f} for text '{text}'")

    array = wav.view(-1).cpu().numpy()
    return array
